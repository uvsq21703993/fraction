package uvsq.fr.wg;

import static org.junit.Assert.*;

import org.junit.Test;

public class FractionTest {

	@Test
	public void testInitialisation() {
		Fraction F1, F2, F3;
		F1 = new Fraction (5, 3);
		F2 = new Fraction (9);
		F3 = new Fraction ();
	}

	@Test
	public void testConsultation() {
		Fraction F1 = new Fraction (2, 7);
		System.out.println("numerateur: " + F1.getNum() + " || dénominateur: " + F1.getDen());
	}
	
	@Test
	public void testString() {
		Fraction F1 = new Fraction (2, 7);
		System.out.println(F1.getString());
	}
}
